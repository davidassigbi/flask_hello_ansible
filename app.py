from flask import Flask, jsonify
from datetime import datetime
import platform

app = Flask(__name__)

server_name = platform.node()
app_instance_name = 'myapp'
app_server_name = server_name
student_name = 'David'

@app.route("/")
def hello():
    return '<h1>Hello from {} on server {}!</h1>'.format(app_instance_name, app_server_name)

@app.route("/timestamp")
def timestamp():
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    return jsonify({'time': dt_string})

@app.route("/host")
def host():
    return jsonify({'server_name': server_name, 'student_name': student_name})


@app.route("/health")
def health():
    return jsonify({'status': 'ok'})

if __name__ == "__main__":
    app.run(host='0.0.0.0')
